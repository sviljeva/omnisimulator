import robot
import supervisor as exe
import go_to_goal as gtg
import time
import math
from threading import Thread
import plot

# Robot specification
wheel_radius = 0.02
base_length = 0.1
cpr = 240.0
min_rpm = 30
max_rpm = 103
v_max = 2 * math.pi / 60 * max_rpm * wheel_radius

# goal
x_goal = 1
y_goal = 1

# params
k_p = 1
k_i = 0.2
k_d = 0.2

milliseconds = lambda: int(round(time.time() * 1000))
last_time = milliseconds()
current_time = milliseconds()
dt = 0


def watch_time():
    global last_time, current_time, dt
    last_time = current_time
    current_time = milliseconds()
    diff = current_time - last_time
    if diff == 0:
        diff = 1
        time.sleep(0.001)

    dt = diff / 1000
    print('dt: ' + str(dt))


x_est = 0
y_est = 0
phi_est = 0
x_math = 0
y_math = 0
phi_math = 0
to_stop = False
change = False

def simulation():
    mathbot = robot.Robot(robot.State(), robot.Specs(wheel_radius, base_length, cpr, v_max, min_rpm, max_rpm))
    go_to_goal_ctrl = gtg.GoToGoal(gtg.Goal(x_goal,y_goal))
    supervisor = exe.Supervisor(mathbot, go_to_goal_ctrl)

    global last_time, current_time
    last_time = milliseconds()
    current_time = milliseconds()
    global x_est, y_est, phi_est, x_math, y_math, phi_math
    global change
    while not supervisor.to_stop:
        watch_time()
        supervisor.execute(dt)

        x_est = supervisor.estimated_state.x
        y_est = supervisor.estimated_state.y
        phi_est = supervisor.estimated_state.phi

        x_math = mathbot.state.x
        y_math = mathbot.state.y
        phi_math = mathbot.state.phi
        change = True
    global to_stop
    to_stop = True


if __name__ == '__main__':
    d = plot.DynamicPlot()
    est_state = plot.OmniPatches(0, 0, 0, base_length, wheel_radius, "#FF8C00")
    math_state = plot.OmniPatches(0, 0, 0, base_length, wheel_radius,"#00BFFF")
    d.add_omni_patches(est_state)
    d.add_omni_patches(math_state)
    sim = Thread(target=simulation)
    sim.start()
    while not to_stop:
        if change:
            est_state.on_running(x_est, y_est, phi_est)
            math_state.on_running(x_math, y_math, phi_math)
            d.on_running()
            time.sleep(0.00001)
            change = False
    print('End')
