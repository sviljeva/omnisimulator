import math

class Specs:
    __slots__ = 'wheel_radius', 'base_length', 'cpr', 'v_max', 'min_rpm', 'max_rpm', 'angular_wheel_min', 'angular_wheel_max'

    def __init__(self, wheel_radius, base_length, cpr, v_max, min_rpm, max_rpm):
        self.wheel_radius = wheel_radius
        self.base_length = base_length
        self.cpr = cpr
        self.v_max = v_max
        self.min_rpm = min_rpm
        self.max_rpm = max_rpm
        self.angular_wheel_min = self.min_rpm * 2 * math.pi / 60
        self.angular_wheel_max = self.max_rpm * 2 * math.pi / 60

class State:
    __slots__ = 'x', 'y', 'phi'

    def __init__(self,x=0,y=0,phi=-math.pi/2):
        self.x=x
        self.y=y
        self.phi=phi

class Wheel:
    __slots__ = 'nr_of_ticks', 'nr_of_revolutions', 'cpr', 'wheel_radius'

    def __init__(self, wheel_radius, cpr):
        self.nr_of_ticks = 0
        self.nr_of_revolutions = 0
        self.wheel_radius = wheel_radius
        self.cpr = cpr

    def update_ticks(self, v, dt):
        self.nr_of_revolutions += v*dt/2/math.pi
        self.nr_of_ticks = int(self.nr_of_revolutions*self.cpr)

class Robot:
    __slots__ = 'state', 'specs', 'wheel_1', 'wheel_2', 'wheel_3'

    def __init__(self, state, specs):
        self.state=state
        self.specs=specs
        self.wheel_1:Wheel=Wheel(specs.wheel_radius,specs.cpr)
        self.wheel_2:Wheel=Wheel(specs.wheel_radius,specs.cpr)
        self.wheel_3:Wheel=Wheel(specs.wheel_radius,specs.cpr)

    def omni_to_uni(self, phi1_dot, phi2_dot, phi3_dot):
        x_dot=(self.specs.wheel_radius*math.sqrt(3)/3)*(phi2_dot-phi3_dot)
        y_dot=(self.specs.wheel_radius/3)*(phi3_dot+phi2_dot-2*phi1_dot)
        theta_dot=-self.specs.wheel_radius*(phi1_dot+phi2_dot+phi3_dot)/(3*self.specs.base_length)
        return x_dot,y_dot,theta_dot

    def uni_to_omni(self, x_dot, y_dot, theta_dot):
        phi1_dot=(1/self.specs.wheel_radius)*(-y_dot-self.specs.base_length*theta_dot)
        phi2_dot=(1/self.specs.wheel_radius)*(x_dot*math.sqrt(3)/2+y_dot*1/2-self.specs.base_length*theta_dot)
        phi3_dot=(1/self.specs.wheel_radius)*(-x_dot*math.sqrt(3)/2+y_dot*1/2-self.specs.base_length*theta_dot)
        return phi1_dot, phi2_dot, phi3_dot

    def update_state(self, x_dot, y_dot, phi_dot, dt):
        self.state.x+=x_dot*dt
        self.state.y+=y_dot*dt
        self.state.phi+=phi_dot*dt

    def move(self, angular_1,angular_2,angular_3, dt):
        x_dot, y_dot, theta_dot = self.omni_to_uni(angular_1,angular_2,angular_3)
        self.update_state(x_dot,y_dot,theta_dot, dt)
        self.wheel_1.update_ticks(angular_1, dt)
        self.wheel_2.update_ticks(angular_2, dt)
        self.wheel_3.update_ticks(angular_3, dt)
