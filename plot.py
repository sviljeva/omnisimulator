import matplotlib.pyplot as plt
import math

import numpy

plt.ion()


def cart_to_polar(coord):
    x = coord[0]
    y = coord[1]
    r = math.sqrt(math.pow(x, 2) + math.pow(y, 2))
    phi = math.atan2(y, x)
    return r, phi

def polar_to_cart(coord):
    r = coord[0]
    phi = coord[1]
    x = r * math.cos(phi)
    y = r * math.sin(phi)
    return x, y

def rotate(coord, theta):
    polar = cart_to_polar(coord)
    r = polar[0]
    phi = polar[1]
    phi += theta
    return polar_to_cart((r, phi))

def translate(coord, x, y):
    x_new = coord[0] + x
    y_new = coord[1] + y
    return x_new, y_new

def wheel(point, phi, r):
    p1_0 = rotate((0, -r), phi)
    p2_0 = rotate((0.01, -r), phi)
    p3_0 = rotate((0.01, r), phi)
    p4_0 = rotate((0, r), phi)

    p1 = translate(p1_0, point[0], point[1])
    p2 = translate(p2_0, point[0], point[1])
    p3 = translate(p3_0, point[0], point[1])
    p4 = translate(p4_0, point[0], point[1])

    return numpy.array([list(p1), list(p2), list(p3), list(p4)])

def wheels(x, y, phi, r, l):
    phi -= math.pi / 2
    p1_0 = rotate((l / 2, 0), phi)
    p2_0 = rotate((l/2*math.cos(2*math.pi/3),l/2*math.sin(2*math.pi/3)), phi)
    p3_0 = rotate((l/2*math.cos(4*math.pi/3),l/2*math.sin(4*math.pi/3)), phi)
    

    p1 = translate(p1_0, x, y)
    p2 = translate(p2_0, x, y)
    p3 = translate(p3_0, x, y)

    wheel_1 = wheel(p1, phi, r)
    wheel_2 = wheel(p2, phi+2*math.pi/3, r)
    wheel_3 = wheel(p3, phi+4*math.pi/3, r)

    return wheel_1, wheel_2, wheel_3


class OmniPatches:

    def __init__(self, x, y, phi, base_length, wheel_radius, color):
        self.base_length = base_length
        self.color = color
        self.wheel_radius = wheel_radius

        wheels_points = wheels(x, y, phi, wheel_radius, base_length)

        self.chassis = plt.Circle((0,0),0.048, color=color, alpha=1)
        self.patch1 = plt.Polygon(wheels_points[0], color="#778899", alpha=1)
        self.patch2 = plt.Polygon(wheels_points[1], color="#778899", alpha=1)
        self.patch3 = plt.Polygon(wheels_points[2], color="#778899", alpha=1)

    def on_running(self, x, y, phi):
        wheels_points = wheels(x, y, phi, self.wheel_radius, self.base_length)
        self.chassis = plt.Circle((x,y),0.048, color=self.color, alpha=1)
        self.patch1 = plt.Polygon(wheels_points[0], color="#778899", alpha=1)
        self.patch2 = plt.Polygon(wheels_points[1], color="#778899", alpha=1)
        self.patch3 = plt.Polygon(wheels_points[2], color="#778899", alpha=1)

    def get_patches(self):
        return self.patch1, self.patch2, self.patch3, self.chassis

class DynamicPlot:
    #Suppose we know the x range
    min_x = -1.5
    max_x = 1.5
    min_y = -1.5
    max_y = 1.5

    def __init__(self, set_lims=True):
        self.figure, self.ax = plt.subplots(1, 1)
        #Autoscale on unknown axis and known lims on the other
        self.ax.set_autoscaley_on(True)
        if set_lims:
            self.ax.set_xlim(self.min_x, self.max_x)
            self.ax.set_ylim(self.min_y, self.max_y)
        #Other stuff
        self.ax.grid()
        self.ax.set_axisbelow(True)
        self.ax.set_aspect('equal')
    #    mng = plt.get_current_fig_manager()
    #    mng.window.showMaximized()
        self.omni_patches: list = []

    def add_omni_patches(self, omni_patches):
        self.omni_patches.append(omni_patches)

    def on_running(self):
        self.ax.patches.clear()

        for omni_patch in self.omni_patches:
            for patch in omni_patch.get_patches():
                self.ax.add_patch(patch)

        #Need both of these in order to rescale
        self.ax.relim()
        self.ax.autoscale_view()
        #We need to draw *and* flush
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()