import math
import go_to_goal
import robot


class Supervisor:
    __slots__ = 'robot', 'go_to_goal_ctrl', 'to_stop', 'estimated_state', 'ticks_1', 'ticks_2','ticks_3', 'distance_from_goal'

    def __init__(self, robot_ref, go_to_goal_ctrl):
        self.robot = robot_ref
        self.go_to_goal_ctrl: go_to_goal.GoToGoal = go_to_goal_ctrl
        self.to_stop = False
        self.estimated_state: robot.State = robot.State()
        self.ticks_1=0
        self.ticks_2=0
        self.ticks_3=0
        self.distance_from_goal = 0

    def calculate_distance_from_goal(self):
        x_goal = self.go_to_goal_ctrl.goal.x
        y_goal = self.go_to_goal_ctrl.goal.y
        self.distance_from_goal = math.sqrt(math.pow(self.estimated_state.x - x_goal, 2) + math.pow(self.estimated_state.y - y_goal, 2))

    def estimate_position(self):

        dt_1 = self.robot.wheel_1.nr_of_ticks - self.ticks_1
        dt_2 = self.robot.wheel_2.nr_of_ticks - self.ticks_2
        dt_3 = self.robot.wheel_3.nr_of_ticks - self.ticks_3
        self.ticks_1 += dt_1
        self.ticks_2 += dt_2
        self.ticks_3 += dt_3

        x = self.estimated_state.x
        y = self.estimated_state.y
        phi = self.estimated_state.phi

        meters_per_tick = 2*math.pi*self.robot.specs.wheel_radius / self.robot.specs.cpr

        distance_1 = dt_1*meters_per_tick
        distance_2 = dt_2*meters_per_tick
        distance_3 = dt_3*meters_per_tick
        distance_center = (distance_1 + distance_2 + distance_3) / 3

        angle_per_tick=2*math.pi/self.robot.specs.cpr

        phi_dt_1 = angle_per_tick*dt_1
        phi_dt_2 = angle_per_tick*dt_2
        phi_dt_3 = angle_per_tick*dt_3

        x_dot=(self.robot.specs.wheel_radius*math.sqrt(3)/3)*(phi_dt_2-phi_dt_3)
        y_dot=(self.robot.specs.wheel_radius/3)*(phi_dt_3+phi_dt_2-2*phi_dt_1)
        theta_dot=-self.robot.specs.wheel_radius*(phi_dt_1+phi_dt_2+phi_dt_3)/(3*self.robot.specs.base_length)

        self.estimated_state.x += x_dot
        self.estimated_state.y += y_dot
        phi_new = phi + theta_dot
        self.estimated_state.phi = math.atan2(math.sin(phi_new), math.cos(phi_new))

    def at_goal(self):
        return self.distance_from_goal < (self.robot.specs.base_length / 2)


    def ensure_angular_speed(self, x_dot, y_dot, angular_speed):
        radius = self.robot.specs.wheel_radius
        base_length = self.robot.specs.base_length

        angular_wheel_min = self.robot.specs.angular_wheel_min
        angular_wheel_max = self.robot.specs.angular_wheel_max
        transl_speed = x_dot / math.cos(self.estimated_state.phi)

        if abs(transl_speed) > 0:
            v_limit = max(min(abs(transl_speed), (radius / 2)*(2*angular_wheel_max)), (radius / 2)*(2*angular_wheel_min))
            w_limit = max(min(abs(angular_speed), (radius / base_length)*(angular_wheel_max - angular_wheel_min)), 0)

            desired_1, desired_2, desired_3 = self.robot.uni_to_omni(v_limit*math.cos(self.estimated_state.phi), v_limit*math.sin(self.estimated_state.phi),w_limit)

            fst_sec_thrd_max = max(desired_1, desired_2, desired_3)
            fst_sec_thrd_min = min(desired_1, desired_2, desired_3)

            if fst_sec_thrd_max > angular_wheel_max:
                shift_angular_1 = desired_1 - (fst_sec_thrd_max - angular_wheel_max)
                shift_angular_2 = desired_2 - (fst_sec_thrd_max - angular_wheel_max)
                shift_angular_3 = desired_3 - (fst_sec_thrd_max - angular_wheel_max)
            elif fst_sec_thrd_min < angular_wheel_min:
                shift_angular_1 = desired_1 + (angular_wheel_min - fst_sec_thrd_min)
                shift_angular_2 = desired_2 + (angular_wheel_min - fst_sec_thrd_min)
                shift_angular_3 = desired_3 + (angular_wheel_min - fst_sec_thrd_min)
            else:
                shift_angular_1 = desired_1
                shift_angular_2 = desired_2
                shift_angular_3 = desired_3

            shift_x, shift_y, shift_theta = self.robot.omni_to_uni(shift_angular_1, shift_angular_2, shift_angular_3)
            transl_speed = math.copysign(1, transl_speed)*shift_x/math.cos(self.estimated_state.phi)
            angular_speed = math.copysign(1, angular_speed)*shift_theta
        else:
            w_min = radius / base_length * (2 * angular_wheel_min)
            w_max = radius / base_length * (2 * angular_wheel_max)
            if abs(angular_speed) > w_min:
                angular_speed = math.copysign(1, angular_speed)*max(min(abs(angular_speed), w_max), w_min)
            else:
                angular_speed = 0

                x_dot = transl_speed * math.cos(self.estimated_state.phi)		
                y_dot = transl_speed * math.sin(self.estimated_state.phi)

        return x_dot, y_dot, angular_speed

    def execute(self, dt):
        self.estimate_position()
        self.calculate_distance_from_goal()

        x_dot, y_dot, angular_speed = self.go_to_goal_ctrl.execute()
        x_dot_ens, y_dot_ens, angular_speed_ens = self.ensure_angular_speed(x_dot, y_dot, angular_speed)
        phi1_dot, phi2_dot, phi3_dot = self.robot.uni_to_omni(x_dot_ens,y_dot_ens, angular_speed_ens)
        phi1_dot, phi2_dot, phi3_dot = self.robot.uni_to_omni(x_dot,y_dot, angular_speed)
        self.robot.move(phi1_dot, phi2_dot, phi3_dot, dt)

        if self.at_goal():
            self.to_stop = True

