import math

class Goal:
    __slots__ = 'x', 'y'
    def __init__(self,x,y):
        self.x=x
        self.y=y

class GoToGoal:
    __slots__ = 'goal'
    def __init__(self,goal):
        self.goal=goal

    def execute(self):
        x_dot_i=self.goal.x/30
        y_dot_i=self.goal.y/30

        theta_dot=0.6

        return x_dot_i, y_dot_i, theta_dot